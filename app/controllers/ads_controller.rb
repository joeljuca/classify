class AdsController < ApplicationController
  def index
    @payload = Ad.all
    render json: @payload
  end

  def show
    @payload = Ad.find(params[:id])
    render json: @payload
  end

  def create
    @ad_params = allowed_params_only(params)
    @ad = Ad.create(@ad_params)
    head :created, location: ad_path(@ad)
  rescue => exception
    render status: 500, json: {
      message: "Something went wrong while trying to create an ad.",
      errors: exception.to_json
    }
  end

  def update
    @ad = Ad.find(params[:id])
    @ad_params = allowed_params_only(params)

    Ad.update(@ad_params) if @ad
    head :no_content
  end

  def delete
    @ad = Ad.find(params[:id])
    @ad.delete if @ad
    head :no_content
  end

  private

  # Returns a hash containing only parameters used in the Ad model.
  # Usage (from inside a controller method):
  #   @ad_params = allowed_params_only(params)
  #   @ad = Ad.create(@ad_params)
  def allowed_params_only(params)
    @ad_params = {}

    # TODO: find a way to get this list of atoms dynamically
    [:id, :title, :desc, :is_active].each do |key|
      @ad_params[key] = params[key] if params[key]
    end

    @ad_params
  end
end
